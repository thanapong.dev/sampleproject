import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from './app.routes'; // Import your routes configuration
import { Router } from '@angular/router';
import { AppComponent } from './app.component'

describe('Router', () => {
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)], 
    });
    router = TestBed.inject(Router); 
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have the 'front-end' title`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('front-end');
  });

  it('should navigate to UserListComponent when navigating to /', () => {
    router.navigateByUrl('/').then(() => {
      expect(router.url).toBe('/');
    });
  });

  it('should navigate to UserListComponent when navigating to /userManagement', () => {
    router.navigateByUrl('/userManagement').then(() => {
      expect(router.url).toBe('/userManagement');
    });
  });

  it('should navigate to UsercrudComponent when navigating to /userManagement2', () => {
    router.navigateByUrl('/userManagement2').then(() => {
      expect(router.url).toBe('/userManagement2');
    });
  });

  it('should navigate to UserEditComponent when navigating to /editUser', () => {
    router.navigateByUrl('/editUser').then(() => {
      expect(router.url).toBe('/editUser');
    });
  });

  it('should navigate to UserAddComponent when navigating to /addUser', () => {
    router.navigateByUrl('/addUser').then(() => {
      expect(router.url).toBe('/addUser');
    });
  });

});
