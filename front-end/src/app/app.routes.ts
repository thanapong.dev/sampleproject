import { Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UsercrudComponent } from './usercrud/usercrud.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserAddComponent } from './user-add/user-add.component';

export const routes: Routes = [
    { path: '', component: UserListComponent },
    { path: 'userManagement', component: UserListComponent },
    { path: 'userManagement2', component: UsercrudComponent },
    { path: 'editUser', component: UserEditComponent },
    { path: 'addUser', component: UserAddComponent },
];
