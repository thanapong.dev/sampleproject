import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

interface DialogData {
  name: string;
}

@Component({
  selector: 'app-delete-modal',
  standalone: true,
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {
  firstName: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    // this.firstName = data;
    console.log('data: DialogData',this.data)
  }
  closeModal(){
    console.log('data: DialogData',this.data)
  }
  ngOnInit(): void {
  }
}
