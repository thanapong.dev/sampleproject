import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'; // Import HttpResponse

@Component({
  selector: 'app-user-add',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './user-add.component.html',
  styleUrl: './user-add.component.scss'
})
export class UserAddComponent {
  name: String = "";
  email: String = "";
  tel: String = "";
  address: String = "";
  userData: string = ''
  validateInput: boolean = false
  invalidEmail: boolean = false
  invalidTel: boolean = false

  constructor(private router: Router, private http: HttpClient) { }

  cancelUser() {
    this.router.navigate(['/userManagement'])
  }
  addUser() {
    this.validateInput = true
    if (this.validate()) {
      let body = {
        name: this.name,
        email: this.email,
        tel: this.tel,
        address: this.address,
      }
      this.http.post("http://localhost:4000/users/", body)
        .subscribe((resultData: any) => {
          this.router.navigate(['/userManagement'])
        });
    }
  }
  validate() {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!this.email.match(mailformat)) {
      this.invalidEmail = true
    }
    if (isNaN(Number(this.tel))) {
      this.invalidTel = true
    }
    if (this.name != "" && this.email != "" && this.tel != "" && this.address != "" && this.email.match(mailformat) && !isNaN(Number(this.tel))) {
      return true
    } else {
      return false
    }
  }
  invalidEmailInput() {
    this.invalidEmail = false
    this.invalidTel = false

  }
}
