import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent {
  name: string = "";
  email: string = "";
  tel: string = "";
  address: string = "";
  userData: string = '';
  invalidEmail: boolean = false
  invalidTel: boolean = false

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    this.getUserDetail();
  }

  getUserDetail() {
    var userID = this.route.snapshot.queryParams['id']
    this.http.get("http://localhost:4000/users/" + userID, { observe: 'response' })
      .subscribe((response: HttpResponse<any>) => {
        console.log('response.status', response)
        if (response.status === 200) {
          const resultData = response.body;
          this.name = resultData.name;
          this.email = resultData.email;
          this.tel = resultData.tel;
          this.address = resultData.address;
          console.log('Test', resultData);
        } else {
          console.error('Server returned error status:', response.status);
        }
      }, (error) => {
        console.error('HTTP Error:', error);
        this.router.navigate(['/userManagement'])
      });
  }

  editUser() {
    if (this.validate()) {
      var userID = this.route.snapshot.queryParams['id']
      let body = {
        name: this.name,
        email: this.email,
        tel: this.tel,
        address: this.address,
      }
      this.http.patch("http://localhost:4000/users/" + userID, body)
        .subscribe((resultData: any) => {
          console.log('Test', resultData);
          this.router.navigate(['/userManagement'])
        });
    }
  }

  validate() {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!this.email.match(mailformat)) {
      this.invalidEmail = true
    }
    if (isNaN(Number(this.tel))) {
      this.invalidTel = true
    }
    if (this.name != "" && this.email != "" && this.tel != "" && this.address != "" && this.email.match(mailformat) && !isNaN(Number(this.tel))) {
      return true
    } else {
      return false
    }
  }
  cancel() {
    this.router.navigate(['/userManagement'])
  }

  invalidEmailInput(){
    this.invalidEmail = false
    this.invalidTel = false

  }

}
