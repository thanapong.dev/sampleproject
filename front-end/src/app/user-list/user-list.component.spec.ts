import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserListComponent } from './user-list.component';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('UserListComponent', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule], 
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should fetch user data on initialization', () => {
    const fixture = TestBed.createComponent(UserListComponent);
    const component = fixture.componentInstance;
    const testData = [{ id: 1, name: 'Test User' }];
    const req = httpTestingController.expectOne('http://localhost:4000/users/getAll');
    expect(req.request.method).toEqual('GET');
    req.flush(testData);
    fixture.detectChanges();
    expect(component.UserArray).toEqual(testData);

    console.log('Request Data:', req.request);
    console.log('Response Data:', req.request.body); 
  });

  it('should handle error if backend server is not available', () => {
    const fixture = TestBed.createComponent(UserListComponent);
    const component = fixture.componentInstance;
    const error = { status: 500, statusText: 'Internal Server Error' };

    const req = httpTestingController.expectOne('http://localhost:4000/users/getAll');
    req.error(new ErrorEvent('network error'), { status: error.status });

    fixture.detectChanges();
    expect(component.UserArray).toEqual([]);

    console.log('Error occurred:', error);
  });

  it('should handle empty user data response', () => {
    const fixture = TestBed.createComponent(UserListComponent);
    const component = fixture.componentInstance;

    const req = httpTestingController.expectOne('http://localhost:4000/users/getAll');
    req.flush([]);

    fixture.detectChanges();
    expect(component.UserArray).toEqual([]);

    console.log('Empty response received');
  });
});
