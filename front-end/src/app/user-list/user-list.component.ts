import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA,MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-user-list',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.scss'
})
export class UserListComponent {
  UserArray: any[] = [];
  userId: String = "";
  search : string = ""
  constructor(private http: HttpClient, private route: Router, private dialogRef: MatDialog) {
    this.getAllStudent();
  }

  getAllStudent() {
    this.http.get("http://localhost:4000/users/getAll")
      .subscribe((resultData: any) => {
        this.UserArray = resultData
      });
  }

  editUser(id: string) {
    this.route.navigate(['/editUser'], { queryParams: { id } })
  }

  addUser() {
    this.route.navigate(['/addUser'])

  }

  delUser(id: string) {
    global = id
    // this.dialogRef.open(DeleteModalComponent, {
    //   height: '30%',
    //   width: '30%'
    // })
    if (confirm("Are you sure to delete ")) {
      this.http.delete("http://localhost:4000/users/" + id)
        .subscribe((resultData: any) => {
          console.log('Test', resultData);
          this.getAllStudent();
        });
    }
  }

  searchUser(){
    console.log('this.search',this.search)
  }

}

interface DialogData {
}
var global: string = ''
@Component({
  selector: 'app-delete-modal',
  standalone: true,
  templateUrl: 'delete-modal.component.html',
  styleUrl: './user-list.component.scss'
})
export class DeleteModalComponent {
  constructor(private dialogRef: MatDialogRef<DeleteModalComponent>) {
  }

  closeModal() {
    this.dialogRef.close();
  }
  delUser(){
  }
}
