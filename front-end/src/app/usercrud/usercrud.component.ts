import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router,RouterLink } from '@angular/router';

@Component({
  selector: 'app-usercrud',
  standalone: true,
  imports: [CommonModule, FormsModule, RouterLink],
  templateUrl: './usercrud.component.html',
  styleUrl: './usercrud.component.scss'
})
export class UsercrudComponent {
  name: String = "";
  email: String = "";
  tel: String = "";
  address: String = "";
  userData: string = ''
  UserArray: any[] = [];
  userId: String = "";
  constructor(private http: HttpClient,private route: Router) {
    this.getAllStudent();
  }
  getAllStudent() {
    this.http.get("http://localhost:4000/users/getAll")
      .subscribe((resultData: any) => {
        this.UserArray = resultData
        console.log('Test', resultData);
      });
  }
  async setUpdate() {
    if (this.name != "" && this.email != "" && this.tel != "" && this.address != "") {
      if (this.userId == '') {
        let body = {
          name: this.name,
          email: this.email,
          tel: this.tel,
          address: this.address,
        }
        this.http.post("http://localhost:4000/users/", body)
          .subscribe((resultData: any) => {
            console.log('Test', resultData);
            this.getAllStudent();
          });
      } else {
        let body = {
          name: this.name,
          email: this.email,
          tel: this.tel,
          address: this.address,
        }
        this.http.patch("http://localhost:4000/users/" + this.userId, body)
          .subscribe((resultData: any) => {
            console.log('Test', resultData);
            this.getAllStudent();
          });
        this.userId = ''
      }
      this.name = "";
      this.email = "";
      this.tel = "";
      this.address = "";
      console.log('Test', this.name)
      this.getAllStudent();

    }
  }
  editUser(id: string) {
    console.log('id', id)
    this.userId = id

    this.http.get("http://localhost:4000/users/" + this.userId)
      .subscribe((resultData: any) => {
        this.name = resultData.name
        this.email = resultData.email
        this.tel = resultData.tel
        this.address = resultData.address

        console.log('Test', resultData);
      });
  }

  delUser(id: string) {
    this.userId = id
    if (confirm("Are you sure to delete ")) {
      this.http.delete("http://localhost:4000/users/" + this.userId)
        .subscribe((resultData: any) => {
          console.log('Test', resultData);
          this.getAllStudent();
        });
      this.userId = ''
    }

  }

  editUserR(id: string) {
    this.route.navigate(['/editUser'],{queryParams:{id}})
  }
}
